import fs from 'fs';
import path from 'path';
import merger from 'json-schema-merge-allof';
import refparser from 'json-schema-ref-parser';
import util from 'util';

if (process.argv.length != 3) {
  console.log("Schema file is required");
  process.exit(1);
}

const schemaFile = process.argv[2];

try {
  const stat = fs.statSync(schemaFile);
} catch (e) {
  console.log("Error reading schema file:", e.code);
  process.exit(e.errno);
}

(async () => {
  const f = schemaFile;
  const d = fs.readFileSync(path.join(path.dirname(f), "vulnerability-details-format.json"));
  const p = new refparser();
  const o = await p.dereference(f, { resolve: { ignoredExternalRefs: ["vulnerability-details-format.json"] } });
  const r = merger(o);
  r.definitions = JSON.parse(d).definitions;
  r.properties.vulnerabilities.items.properties.details = { "$ref": "#/definitions/named_list/properties/items" };
  console.log(JSON.stringify(r));
})();
