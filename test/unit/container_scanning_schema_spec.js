import b from './builders/index'
import {schemas} from './support/schemas'

describe('container scanning schema', () => {

  it('should validate location', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).success).toBeTruthy()
  })

  it('location dependency is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors).toContain('location requires property "dependency"')
  })

  it('location os is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors).toContain('location requires property "operating_system"')
  })

  it('location image is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors).toContain('location requires property "image"')
  })
})
