#!/bin/bash

bash_unit test/test-container-scanning.sh \
          test/test-sast.sh \
          test/test-coverage-fuzzing.sh \
          test/test-dependency-scanning.sh \
          test/test-dast.sh \
          test/test-secret-detection.sh

echo
echo "Running Javascript specs..."
npx jasmine --require=esm --config=test/unit/support/jasmine.json
