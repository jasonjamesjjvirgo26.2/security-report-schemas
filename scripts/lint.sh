#!/usr/bin/env bash

set -e

os=""
case $(uname -a) in
  Linux*) os="linux" ;;
  Darwin*) os="mac" ;;
  *)
    echo "unsupported os"
    exit 1
    ;;
esac

# shellcheck disable=SC2044
for i in $(find src -type f); do
  echo -n "$i: "
  bin/jsonlint-$os <"$i"
done
